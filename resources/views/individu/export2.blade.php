<table>
{{-- @foreach ($data as $guru) --}}
  <thead>
    <tr height="40">
      @foreach ($data as $guru)
        <td height="20" colspan="9" style='text-align: center; font-weight: bold; font-size: 18px;'>REKAPAN PERORANGAN</td>
        <td width="30"></td>
      @endforeach
    </tr>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center; font-weight: bold;" colspan="1">Nama</th>
      <th style="text-align: center; font-weight: bold;" colspan="4"><b>{{ $guru['nama'] }}</b></th>
      <th style="text-align: center; font-weight: bold;">Jabatan</th>
      <th style="text-align: center; font-weight: bold;" colspan="3"><b>{{ $guru['jabatan'] }}</b></th>
      <td width="30"></td>
      @endforeach
    </tr>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center;" colspan="1">Bulan</th>
      <th style="text-align: center; font-weight: bold;" colspan="4">{{ $guru['bulan'] }}</th>
      <th style="text-align: center;">ID</th>
      <th style="text-align: center; font-weight: bold;" colspan="3">{{ $guru['id'] }}</th>
      <td width="30"></td>
      @endforeach
    </tr>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center; font-weight: bold;" rowspan="2">Absen</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Ijin</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Dinas</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Kehadiran (Hari)</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Lembur (Jam)</th>
      <th style="text-align: center; font-weight: bold;" colspan="2">Terlambat</th>
      <th style="text-align: center; font-weight: bold;" colspan="2">Pulang Cepat</th>
      <td width="30"></td>
      @endforeach
    </tr>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center; font-weight: bold;">Jumlah</th>
      <th style="text-align: center; font-weight: bold;">Menit</th>
      <th style="text-align: center; font-weight: bold;">Jumlah</th>
      <th style="text-align: center; font-weight: bold;">Menit</th>
      <td width="30"></td>
      @endforeach
    </tr>
  </thead>
  
  <tbody>
    <tr>
      @foreach ($data as $guru)
      <td style="text-align: center;">{{ $guru['absen'] }}</td>
      <td style="text-align: center;">{{ $guru['ijin'] }}</td>
      <td style="text-align: center;">{{ $guru['dinas'] }}</td>
      <td style="text-align: center;">{{ $guru['kehadiran_actual'] }}</td>
      <td style="text-align: center;">{{ $guru['jam_lembur'] }}</td>
      <td style="text-align: center;">{{ $guru['total_terlambat'] }}</td>
      <td style="text-align: center;">{{ $guru['menit_terlambat'] }}</td>
      <td style="text-align: center;">{{ $guru['total_pulang_cepat'] }}</td>
      <td style="text-align: center;">{{ $guru['menit_pulang_cepat'] }}</td>
      <td width="30"></td>
      @endforeach
    </tr>
  </tbody>
  <thead>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center; font-weight: bold;" colspan="9">Tabel Kehadiran</th>
      <td width="30"></td>
      @endforeach
    </tr>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center; font-weight: bold;" colspan="2" rowspan="2">Tanggal</th>
      <th style="text-align: center; font-weight: bold;" colspan="4">Jam Kerja</th>
      <th style="text-align: center; font-weight: bold;" colspan="3" rowspan="2">Lembur (Jam)</th>
      <td width="30"></td>
      @endforeach
    </tr>
    <tr>
      @foreach ($data as $guru)
      <th style="text-align: center; font-weight: bold;" colspan="2">Masuk</th>
      <th style="text-align: center; font-weight: bold;" colspan="2">Keluar</th>
      <th></th>
      @endforeach
    </tr>
  </thead>
  <tbody>
    {{-- @foreach ($data as $i=> $guru) --}}
      {{-- @foreach ($data[$i]['time']['Datang'] as $key => $item) --}}
      @for($i=0;$i<=31;$i++)
      @php  $now = $i+1; @endphp
      <tr>
        {{-- @foreach($data as $index=> $user) --}}
        @foreach($data as $index=> $user)
          @if(!empty($data[$index]['time']['Datang'][$i]))
            <td style="text-align: center;" colspan="2">{{ date('l/j', strtotime($data[$index]['time']['Datang'][$i])) }}</td>
            <td style="text-align: center;" colspan="2">{{ date('H:i:s', strtotime($data[$index]['time']['Datang'][$i])) }}</td>
            <td style="text-align: center;" colspan="2">{{  date('H:i:s', strtotime($data[$index]['time']['Pulang'][$i])) }}</td>
            <td style="text-align: center;" colspan="3">
              @php
                if(date('H:i:s', strtotime($data[$index]['time']['Pulang'][$i])) > $batas_lembur){
                    $jam_lembur = strtotime(date("H:i:s", strtotime($data[$index]['time']['Pulang'][$i]))) - strtotime(date($jam_pulang));
                    echo floor($jam_lembur / (60*60));
                }else{
                  echo 0;
                }
              @endphp
            </td>
            <td></td>
          @else
            <td colspan="2"></td>
            <td colspan="7"></td>
            <td></td>
          @endif
        @endforeach
        </tr>
      @endfor
    {{-- @endforeach --}}
  </tbody>
  <tr></tr>
  <tr></tr>
  <tr></tr>
  {{-- @endforeach --}}
</table>
