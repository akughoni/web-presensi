<table>
@foreach ($data as $guru)
  <thead>
    <tr height="40">
      <td height="20" colspan="9" style='text-align: center; font-weight: bold; font-size: 18px;'>REKAPAN PERORANGAN</td>
    </tr>
    <tr>
      <th style="text-align: center; font-weight: bold;" colspan="1">Nama</th>
      <th style="text-align: center; font-weight: bold;" colspan="4"><b>{{ $guru['nama'] }}</b></th>
      <th style="text-align: center; font-weight: bold;">Jabatan</th>
      <th style="text-align: center; font-weight: bold;" colspan="3"><b>{{ $guru['jabatan'] }}</b></th>
    </tr>
    <tr>
      <th style="text-align: center;" colspan="1">Bulan</th>
      <th style="text-align: center; font-weight: bold;" colspan="4">{{ $guru['bulan'] }}</th>
      <th style="text-align: center;">ID</th>
      <th style="text-align: center; font-weight: bold;" colspan="3">{{ $guru['id'] }}</th>
    </tr>
    <tr>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Absen</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Ijin</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Dinas</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Kehadiran (Hari)</th>
      <th style="text-align: center; font-weight: bold;" rowspan="2">Lembur (Jam)</th>
      <th style="text-align: center; font-weight: bold;" colspan="2">Terlambat</th>
      <th style="text-align: center; font-weight: bold;" colspan="2">Pulang Cepat</th>
    </tr>
    <tr>
      <th style="text-align: center; font-weight: bold;">Jumlah</th>
      <th style="text-align: center; font-weight: bold;">Menit</th>
      <th style="text-align: center; font-weight: bold;">Jumlah</th>
      <th style="text-align: center; font-weight: bold;">Menit</th>
    </tr>
  </thead>
  
  <tbody>
    <tr>
      <td style="text-align: center;">{{ $guru['absen'] }}</td>
      <td style="text-align: center;">{{ $guru['ijin'] }}</td>
      <td style="text-align: center;">{{ $guru['dinas'] }}</td>
      <td style="text-align: center;">{{ $guru['kehadiran_actual'] }}</td>
      <td style="text-align: center;">{{ $guru['jam_lembur'] }}</td>
      <td style="text-align: center;">{{ $guru['total_terlambat'] }}</td>
      <td style="text-align: center;">{{ $guru['menit_terlambat'] }}</td>
      <td style="text-align: center;">{{ $guru['total_pulang_cepat'] }}</td>
      <td style="text-align: center;">{{ $guru['menit_pulang_cepat'] }}</td>
    </tr>
  </tbody>
  <thead>
    <tr>
      <th style="text-align: center; font-weight: bold;" colspan="9">Tabel Kehadiran</th>
    </tr>
    <tr>
      <th style="text-align: center; font-weight: bold;" colspan="2" rowspan="2">Tanggal</th>
      <th style="text-align: center; font-weight: bold;" colspan="4">Jam Kerja</th>
      <th style="text-align: center; font-weight: bold;" colspan="3" rowspan="2">Lembur (Jam)</th>
    </tr>
    <tr>
      <th style="text-align: center; font-weight: bold;" colspan="2">Masuk</th>
      <th style="text-align: center; font-weight: bold;" colspan="2">Keluar</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($guru['time']['Datang'] as $key => $item)
    <tr>
      <td style="border:1px solid black; text-align: center;" colspan="2">{{ date('l/j', strtotime($item)) }}</td>
      <td style="border:1px solid black; text-align: center;" colspan="2">{{ date('H:i:s', strtotime($item)) }}</td>
      <td style="border:1px solid black; text-align: center;" colspan="2">{{  date('H:i:s', strtotime($guru['time']['Pulang'][$key]))  }}</td>
      <td style="border:1px solid black; text-align: center;" colspan="3">
        @php
        if(date('H:i:s', strtotime($guru['time']['Pulang'][$key])) > $batas_lembur){
            $jam_lembur = strtotime(date("H:i:s", strtotime($guru['time']['Pulang'][$key]))) - strtotime(date($jam_pulang));
            echo floor($jam_lembur / (60*60));
        }else{
          echo 0;
        }
        @endphp
      </td>
    </tr>
    @endforeach
  </tbody>
  <tr></tr>
  <tr></tr>
  <tr></tr>
  @endforeach
</table>
