@extends('component.master')
@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800"></h1>
  {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> --}}

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row">
      <p class="font-weight-bold text-primary col-2">Presensi Guru</p>
      <form class="form-inline col-3" method="GET" action="{{ route("individu.export") }}">
        {{-- <label class="sr-only" for="inlineFormInputName2">Name</label> --}}
        <select class="form-control form-control mb-2 mr-sm-2 col" name="bulan">
          <option value="1">Januari</option>
          <option value="2">Februari</option>
          <option value="3">Maret</option>
          <option value="4">April</option>
          <option value="5">Mei</option>
          <option value="6">Juni</option>
          <option value="7">Juli</option>
          <option value="8">Agustus</option>
          <option value="9">September</option>
          <option value="10">Oktober</option>
          <option value="11">Novermber</option>
          <option value="12">Desember</option>
        </select>
        <button type="submit" class="btn btn-primary mb-2">Export</button>
      </form>
      <form class="form-inline col-5 ml-auto mr-0" method="GET" action="{{ route("individu.data") }}">
        {{-- <label class="sr-only" for="inlineFormInputName2">Name</label> --}}
        <select class="form-control form-control mb-2 mr-sm-2 col" name="guru">
          @foreach ($users as $item)
          <option value="{{ $item->id }}" >{{ $item->nama }}</option>    
          @endforeach
        </select>
        <select class="form-control form-control mb-2 mr-sm-2 col" name="bulan">
          <option value="1">Januari</option>
          <option value="2">Februari</option>
          <option value="3">Maret</option>
          <option value="4">April</option>
          <option value="5">Mei</option>
          <option value="6">Juni</option>
          <option value="7">Juli</option>
          <option value="8">Agustus</option>
          <option value="9">September</option>
          <option value="10">Oktober</option>
          <option value="11">Novermber</option>
          <option value="12">Desember</option>
        </select>
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
      </form>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        @if(!empty($guru))
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="text-center">
            <tr>
              <th colspan="1">Nama</th>
              <th colspan="4" class="text-primary">{{ $guru['nama'] }}</th>
              <th>Jabatan</th>
              <th colspan="3" class="text-primary">{{ $guru['jabatan'] }}</th>
            </tr>
            <tr>
              <th colspan="1">Bulan</th>
              <th colspan="4" class="text-primary">{{ $guru['bulan'] }}</th>
              <th>ID</th>
              <th colspan="3" class="text-primary">{{ $guru['id'] }}</th>
            </tr>
            <tr>
              <th rowspan="2">Absen</th>
              <th rowspan="2">Ijin</th>
              <th rowspan="2">Dinas</th>
              <th rowspan="2">Kehadiran (Hari)</th>
              <th rowspan="2">Lembur (Jam)</th>
              <th colspan="2">Terlambat</th>
              <th colspan="2">Pulang Cepat</th>
            </tr>
            <tr>
              <th>Jumlah</th>
              <th>Menit</th>
              <th>Jumlah</th>
              <th>Menit</th>
            </tr>
          </thead>
          <tbody class="text-center text-primary">
            <tr>
              <td>{{ $guru['absen'] }}</td>
              <td>{{ $guru['ijin'] }}</td>
              <td>{{ $guru['dinas'] }}</td>
              <td>{{ $guru['kehadiran_actual'] }}</td>
              <td>{{ $guru['jam_lembur'] }}</td>
              <td>{{ $guru['total_terlambat'] }}</td>
              <td>{{ $guru['menit_terlambat'] }}</td>
              <td>{{ $guru['total_pulang_cepat'] }}</td>
              <td>{{ $guru['menit_pulang_cepat'] }}</td>
            </tr>
          </tbody>
          <thead class="text-center">
            <tr>
              <th colspan="9" class="text-center">Tabel Kehadiran</th>
            </tr>
            <tr>
              <th colspan="2" rowspan="2">Tanggal</th>
              <th colspan="4">Jam Kerja</th>
              <th colspan="3" rowspan="2">Lembur (Jam)</th>
            </tr>
            <tr>
              <th colspan="2">Masuk</th>
              <th colspan="2">Keluar</th>
            </tr>
          </thead>
          <tbody class="text-center">
            @foreach ($jam['Datang'] as $key => $item)
            <tr>
              <td colspan="2">{{ date('l/j', strtotime($item)) }}</td>
              <td colspan="2">{{ date('H:i:s', strtotime($item)) }}</td>
              <td colspan="2">{{  date('H:i:s', strtotime($jam['Pulang'][$key]))  }}</td>
              <td colspan="3">
                @php
                if(date('H:i:s', strtotime($jam['Pulang'][$key])) > $batas_lembur){
                  $jam_lembur  = strtotime(date("H:i:s", strtotime($jam['Pulang'][$key]))) - strtotime(date($jam_pulang));
                  echo floor($jam_lembur / (60*60));
                }else{
                  echo 0;
                }
                @endphp
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @endif
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection

@section('js')

@endsection

{{-- 
  - nama
  - jabatan
  - total jam kerja
  - terlambat
  - pulang cepat
  - lembur
  - hadir
  - absen
  
  --}}