@extends('component.master')
@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800"></h1>
  <div class="card">
    <div class="card-header bg-primary">
      <h1 class="h3 mb-2 text-gray-100">Data Guru</h1>
    </div>
    <div class="card-body row shadow">
      <div class="mb-4 col-7">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead >
              <tr class="text-center text-primary">
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $guru)
              <tr>
                <td> {{ $guru->nama }} </td>
                <td> {{ $guru->jabatan }} </td>
                <td class="text-center">
                  <a href="{{ route('guru.delete', ['id' => $guru->id ]) }}" class="btn-sm btn-danger btn-circle">
                    <i class="fas fa-trash"></i>
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-4 offset-1">
        <h1 class="h3 mb-2 text-gray-800">Tambah Data Guru</h1>
        <form action="{{ route("guru.add") }}" method="post">
          @csrf
          <div class="form-group">
            <label for="exampleFormControlInput1">Nama</label>
            <input name="nama" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama Lengkap">
          </div>
          <div class="form-group">
            <label for="exampleFormControlInput1">Jabatan</label>
            <input name="jabatan" type="text" class="form-control" id="exampleFormControlInput1"
              placeholder="Guru/Staf/Kepala Sekolah">
          </div>
          <div class="form-group ">
            <button class="btn btn-primary" type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection

@section('js')

@endsection

{{-- 
  - nama
  - jabatan
  - total jam kerja
  - terlambat
  - pulang cepat
  - lembur
  - hadir
  - absen
  
  --}}