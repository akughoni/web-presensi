@extends('component.master')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
  </div>
  <div class="card">
    <div class="card-header bg-primary">
      <h5 class="h5 mb-2 text-gray-100">Lihat Absen Hari Ini</h5>
    </div>
    <div class="card-body row shadow">
      <div class="mb-4 col">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead >
              <tr class="text-center text-primary">
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Keterangan</th>
                <th>Jam</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($data as $item)
              <tr>
                <td> {{ $item->user->nama }} </td>
                <td> {{ $item->user->jabatan }} </td>
                <td> {{ $item->keterangan }} </td>
                <td class="text-center">
                  <a href="{{ route('delete.presensi', ['id' => $item->id ]) }}" class="btn-sm btn-danger btn-circle">
                    <i class="fas fa-trash"></i>
                  </a>
                </td>
                <td> {{ $item->time }} </td>
              </tr>
              @empty
              <tr>
                <td colspan="5"> Belum ada data </td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
@endsection

@section('js')

</script>
@endsection