<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
    <div class="sidebar-brand-icon">
      <i class="fas fa-user-clock"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Presensi-BU</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item">
    <a class="nav-link" href="{{ route('presensi.index') }}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Presensi</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('lihat.presensi') }}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Lihat Absen</span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">

  <li class="nav-item">
    <a class="nav-link" href="{{ route('lihat.data') }}">
      <i class="fas fa-table"></i>
      <span>Rekapan Presensi</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('individu.data') }}">
      <i class="fas fa-table"></i>
      <span>Presensi Individu</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route("guru") }}">
      <i class="fas fa-table"></i>
      <span>Data Guru</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->