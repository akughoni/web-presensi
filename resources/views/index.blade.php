@extends('component.master')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    {{-- <a href="" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mx-4"><i
        class="fas fa-download fa-sm text-white-50"></i>Cetak Perguru</a>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
        class="fas fa-download fa-sm text-white-50"></i>Cetak Keseluruhan</a> --}}
  </div>

  <!-- Content Row -->
  <div class="row">
    <div class="col-6">
      <form method="POST" action="{{ route('presensi') }}">
        @csrf
        <div class="form-group">
          <label for="exampleFormControlSelect1">Nama Guru</label>
          <select class="form-control" name="guru">
            @foreach ($users as $item)
            <option value="{{ $item->id }}" >{{ $item->nama }}</option>    
            @endforeach
          </select>
          <span class="mx-2 text-xs">Pilih nama yang ada</span>
        </div>
        <div class="form-group">
          <label for="exampleFormControlSelect1">Keterangan</label>
          <select class="form-control" name="keterangan">
            <option>Datang</option>
            <option>Pulang</option>
          </select>
          <span class="mx-2 text-xs">Masukan keterangan masuk / pulang</span>
        </div>
        <div class="form-group">
          <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
            <input type="text" name="time" class="form-control datetimepicker-input" data-target="#datetimepicker2" />
            <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
          </div>
          <span class="mx-2 text-xs">Masukan tanggal dan jam</span>
          <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    locale: 'id',
                    format: 'YYYY-MM-D H:m:ss'
                });
            });
          </script>
        </div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
          Submit
        </button>
      </form>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
@endsection

@section('js')

</script>
@endsection