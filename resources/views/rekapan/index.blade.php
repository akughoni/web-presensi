@extends('component.master')
@section('content')
<div class="container-fluid">

  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800"></h1>
  {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> --}}

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row">
      <p class="font-weight-bold text-primary col-2">Presensi Guru</p>
      <form class="form-inline col-3" method="GET" action="{{ route("rekapan.export") }}">
        {{-- <label class="sr-only" for="inlineFormInputName2">Name</label> --}}
        <select class="form-control form-control mb-2 mr-sm-2 col" name="bulan">
          <option value="1">Januari</option>
          <option value="2">Februari</option>
          <option value="3">Maret</option>
          <option value="4">April</option>
          <option value="5">Mei</option>
          <option value="6">Juni</option>
          <option value="7">Juli</option>
          <option value="8">Agustus</option>
          <option value="9">September</option>
          <option value="10">Oktober</option>
          <option value="11">Novermber</option>
          <option value="12">Desember</option>
        </select>
        <button type="submit" class="btn btn-primary mb-2">Export</button>
      </form>
      <form class="form-inline col-5 ml-auto mr-0" method="GET" action="{{ route("lihat.data") }}">
        <label class="sr-only" for="inlineFormInputName2">Name</label>
        <select class="form-control form-control mb-2 mr-sm-2 col" name="bulan">
          <option value="1">Januari</option>
          <option value="2">Februari</option>
          <option value="3">Maret</option>
          <option value="4">April</option>
          <option value="5">Mei</option>
          <option value="6">Juni</option>
          <option value="7">Juli</option>
          <option value="8">Agustus</option>
          <option value="9">September</option>
          <option value="10">Oktober</option>
          <option value="11">Novermber</option>
          <option value="12">Desember</option>
        </select>
        <button type="submit" class="btn btn-primary mb-2">Submit</button>
      </form>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="text-center">
            <tr>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Total Jam Kerja (Standart/Actual)</th>
              <th>Terlambat (Jumlah/Menit)</th>
              <th>Pulang Cepat (Jumlah/Menit)</th>
              <th>Lembur (Jam)</th>
              <th>Kehadiran (S/A)</th>
              <th>Absen</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($guru as $item)
            <tr>
              <td>{{ $item['nama'] }} </td>
              <td>{{ $item['jabatan'] }} </td>
              <td class="text-center">{{ $item['jam_kerja_standart'] }}/{{ $item['jam_kerja_actual'] }} </td>
              <td class="text-center">{{ $item['total_terlambat'] }}/{{ $item['menit_terlambat'] }}</td>
              <td class="text-center">{{ $item['total_pulang_cepat'] }}/{{ $item['menit_pulang_cepat'] }}</td>
              <td class="text-center">{{ $item['jam_lembur'] }}</td>
              <td class="text-center">{{ $item['kehadiran_standart'] }}/{{ $item['kehadiran_actual'] }}</td>
              <td class="text-center">{{ $item['absen'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
@endsection

@section('js')

@endsection

{{-- 
  - nama
  - jabatan
  - total jam kerja
  - terlambat
  - pulang cepat
  - lembur
  - hadir
  - absen
  
  --}}