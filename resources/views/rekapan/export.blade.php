<table width="1080" cellpadding="0" cellspacing="0">
	<tr height="40">
		<td class="xl65" height="40" width="1080" colspan="15" style='text-align: center; font-weight: bold; font-size: 24px;'>REKAPAN KEHADIRAN</td>
	</tr>
	<tr height="19" style='height:14.25px;'>
		<td height="19" style='height:14.25px;'>Bulan<span style='mso-spacerun:yes;'>&nbsp;</span></td>
		<td>: {{ $bulan }} {{ $tahun }}</td>
		<td colspan="13"></td>
	</tr>
	<tr height="19" style='height:14.25px;'>
		<td height="19" colspan="15"></td>
	</tr>
	<tr height="19" style='height:14.25px;'>
		<td height="57" rowspan="2"
			style='height:42.75px;border:1px solid black; text-align: center; font-weight: bold;'>No</td>
		<td rowspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Nama</td>
		<td rowspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Jabatan</td>
		<td colspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Jam Kerja</td>
		<td colspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Terlambat</td>
		<td colspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Pulang Cepat</td>
		<td rowspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Lembur (jam)</td>
		<td class="xl70" colspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Hadir</td>
		<td rowspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Absen</td>
		<td rowspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Dinas</td>
		<td rowspan="2" style='border:1px solid black; text-align: center; font-weight: bold;'>
			Ijin</td>
	</tr>
	<tr height="22" style='height:28.50px;'>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Standart</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Actual</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Kali</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Total Menit</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Kali</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Total Menit</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Standart</td>
		<td style='border:1px solid black; text-align: center; font-weight: bold;'>Actual</td>
	</tr>
	@foreach ($data as $key => $item)	
	<tr height="24" style='height:18.00px;mso-height-source:userset;mso-height-alt:360;'>
		<td style='border:1px solid black; text-align: center;'> {{ $key+1 }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['nama'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['jabatan'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['jam_kerja_standart'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['jam_kerja_actual'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['total_terlambat'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['menit_terlambat'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['total_pulang_cepat'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['menit_pulang_cepat'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['jam_lembur'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['kehadiran_standart'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['kehadiran_actual'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['absen'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['dinas'] }} </td>
		<td style='border:1px solid black; text-align: center;'> {{ $item['ijin'] }} </td>
	</tr>
	@endforeach
</table>