<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresensiModel extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\UserModel','user_model_id','id');
    }
}
