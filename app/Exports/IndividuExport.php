<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});

class IndividuExport implements FromView, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $user;
    public $coll;
    public $time;
    public $jam_pulang;
    public $batas_lembur;

    public function __construct($data)
    {
        // $this->user = $data['users'];
        $this->coll = $data['coll'];
        // $this->time = $data['time'];
        $this->jam_pulang = $data['jam_pulang'];
        $this->batas_lembur = $data['batas_lembur'];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->styleCells(
                    'A1:GB40',
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => 'FF000000'],
                            ]
                        ]
                    ]
                            );
            }
        ];
    }

    public function view(): View
    {
        // return view('individu.export');
        // dd($this->coll);
        return view('individu.export2')->with('data', $this->coll)
                                    ->with('jam_pulang', $this->jam_pulang)
                                    ->with('batas_lembur', $this->batas_lembur);
    }
}
