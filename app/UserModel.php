<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $guarded = [];
    // protected $table = "users";

    public function presensi(){
        return $this->hasMany('App\PresensiModel');
    }
}
