<?php

namespace App\Http\Controllers;

use App\Exports\BulananExport;
use App\UserModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ViewDataController extends Controller
{
    public $masuk = "07:00:00";
    public $keluar = "13:30:00";
    public $batas_lembur = "14:30:00";
    public $now_month;
    public $now_year;
    public $day_holiday;
    // public $day_on_month;

    public function __construct()
    {
        $this->day_holiday = "Friday";
        $this->now_month = date('n');
        $this->now_year = date('Y');
        // $this->day_on_month = date('t', mktime(0, 0, 0, $this->now_month, 1, $this->now_year));
    }

    public function getData($req)
    {
        if (empty($req->bulan)) {
            $month = $this->now_month;
        } else {
            $month = $req->bulan;
        }

        $guru = UserModel::whereHas('presensi', function ($query) use ($month) {
            $query->where("bulan", "=", $month);
        })->with(['presensi'=> function ($query) use ($month) {
            $query->where('presensi_models.bulan', '=', $month);
        }])->get();

        $presensi_bulanan = $guru->map(function ($user, $key) {
            //kelompokkan datang dan pulang
            $time = $user->presensi->mapToGroups(function ($item) {
                return [$item->keterangan => $item->time];
            });

            $jam_kerja_actual =  $this->sumJamKerjaActual($time);
            //cek keterlambatan
            $keterlambatan = $this->keterlambatan($time);
            //cek Pulang Cepat
            $pulang_cepat = $this->pulangCepat($time);
            //lembur
            $jam_lembur = $this->jamLembur($time);
            //kehadiran
            $kehadiran = $this->kehadiran($time);

            return [
                'nama' => $user->nama,
                'jabatan' => $user->jabatan,
                'jam_kerja_standart' => $user->presensi->count() * 6.5 / 2,
                'jam_kerja_actual' => $jam_kerja_actual,
                'total_terlambat' => $keterlambatan[0],
                'menit_terlambat' => floor($keterlambatan[1] / 60),
                'total_pulang_cepat' => $pulang_cepat[0],
                'menit_pulang_cepat' => floor($pulang_cepat[1] / 60),
                'jam_lembur' => floor($jam_lembur / (60*60)),
                'kehadiran_standart' => $kehadiran[0],
                'kehadiran_actual' => $kehadiran[1],
                'absen' => $kehadiran[2],
                'dinas' => 0,
                'ijin' => 0
            ];
        });

        return $presensi_bulanan;
    }

    public function index(Request $req)
    {
        $presensi_bulanan = $this->getData($req);

        return view('rekapan.index')->with('guru', $presensi_bulanan);
    }

    public function export(Request $req)
    {
        $bulan = date('F', mktime(0, 0, 0, $req->bulan, 1, $this->now_year));
        $data = $this->getData($req);
        $tahun = 2020;
        
        // return view('rekapan.export')->with('data', $data)->with('bulan', $bulan)->with('tahun', $tahun);
        return Excel::download(new BulananExport($data, $bulan, $this->now_year), 'Rekapan Bulanan.xlsx');
    }

    public function sumJamKerjaActual($time)
    {
        $coll=collect();
        foreach ($time['Datang'] as $key => $value) {
            $diff = strtotime($time['Pulang'][$key]) - strtotime($value);
            $diff = number_format($diff / (60*60), 1);
            $coll->push($diff);
        }

        return $coll->sum();
    }

    public function keterlambatan($time)
    {
        $terlambat = 0;
        $menit_terlambat = 0;
        foreach ($time['Datang'] as $key => $value) {
            if (date("H:i:s", strtotime($value)) > date($this->masuk)) {
                $terlambat++;
                $menit_terlambat += strtotime(date("H:i:s", strtotime($value))) - strtotime(date($this->masuk));
            }
        }

        return [$terlambat, $menit_terlambat];
    }

    public function pulangCepat($time)
    {
        $pulang_cepat = 0;
        $pulang_cepat_menit = 0;
        foreach ($time['Pulang'] as $key => $value) {
            if (date("H:i:s", strtotime($value)) < date($this->keluar)) {
                $pulang_cepat++;
                $pulang_cepat_menit += strtotime(date($this->keluar)) - strtotime(date("H:i:s", strtotime($value)));
            }
        }

        return [$pulang_cepat, $pulang_cepat_menit];
    }

    public function jamLembur($time)
    {
        $jam_lembur = 0;
        foreach ($time['Pulang'] as $key => $value) {
            if (date("H:i:s", strtotime($value)) > date($this->batas_lembur)) {
                $jam_lembur += strtotime(date("H:i:s", strtotime($value))) - strtotime(date($this->keluar));
            }
        }

        return $jam_lembur;
    }

    public function jumlahLibur($month, $year, $nama_hari)
    {
        $day_on_month = date('t', mktime(0, 0, 0, $month, 1, $year));
        $jumlah_libur = 0; //inisial firday
        for ($i=1;$i<=$day_on_month;$i++) {
            $day = date('l', mktime(0, 0, 0, $month, $i, $year));
            if ($day == $nama_hari) {
                $jumlah_libur++;
            }
        }

        return $jumlah_libur;
    }

    public function kehadiran($time)
    {
        $month_temp = date('n', strtotime($time['Datang'][0]));
        $year_temp = date('Y', strtotime($time['Datang'][0]));
        $day_on_month = date('t', mktime(0, 0, 0, $month_temp, 1, $year_temp));
        $num_holiday = $this->jumlahLibur($month_temp, $year_temp, $this->day_holiday);
        $kehadiran_standart = $day_on_month - $num_holiday;
        $kehadiran_actual = $time['Datang']->count();

        $absen = $kehadiran_standart - $kehadiran_actual;

        return [$kehadiran_standart, $kehadiran_actual, $absen ];
    }
}
