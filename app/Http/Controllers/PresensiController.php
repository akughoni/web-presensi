<?php

namespace App\Http\Controllers;

use App\PresensiModel;
use App\UserModel;
use Illuminate\Http\Request;

class PresensiController extends Controller
{
    public function index()
    {
        $user = UserModel::all();

        return view('index')->with('users', $user);
    }

    public function storeCreate(Request $req)
    {
        $bulan = date('n', strtotime($req->time));
        $data = PresensiModel::create([
            'user_model_id' => $req->guru,
            'keterangan' => $req->keterangan,
            'time' => $req->time,
            'bulan' => $bulan
        ]);

        return redirect('/');
    }

    public function show()
    {
        date_default_timezone_set('Asia/Jakarta'); 
        $date_now = date('Y-m-d');
        
        $data = PresensiModel::where('time', 'like', '%'.$date_now.'%')->with('user')->get();
        // dd($data);
        return view('presensi.index')->with('data', $data);
    }

    public function delete($id)
    {
        $data = PresensiModel::find($id);
        $data->delete();

        return redirect('lihat-absen');
    }
}
