<?php

namespace App\Http\Controllers;

use App\UserModel;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index()
    {
        $data = UserModel::all();

        return view('guru.index')->with("data", $data);
    }

    public function create(Request $req)
    {
        $req->validate([
            'nama' => "required",
            "jabatan" => 'required'
        ]);

        UserModel::create([
            'nama' => $req->nama,
            'jabatan' => $req->jabatan
        ]);

        return redirect()->route('guru');
    }

    public function delete($id)
    {
        $guru = UserModel::find($id);
        $guru->delete();

        return redirect('/guru');
    }
}
