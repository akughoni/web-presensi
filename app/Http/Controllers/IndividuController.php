<?php

namespace App\Http\Controllers;

use App\Exports\IndividuExport;
use App\UserModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class IndividuController extends Controller
{
    public function index(Request $req)
    {
        $proses = new ViewDataController();
        $coll = collect();
        $time = collect();
        if (empty($req->guru)) {
            $month = $proses->now_month;
        } else {
            $month = $req->bulan;
        }

        if (!empty($req->guru)) {
            $guru = UserModel::where('id', $req->guru)->whereHas('presensi', function ($query) use ($month) {
                $query->where("bulan", "=", $month);
            })->with(['presensi'=> function ($query) use ($month) {
                $query->where('presensi_models.bulan', '=', $month);
            }])->first();

            if ($guru == null) {
                return view('individu.index')->with('users', UserModel::all())->with('guru', null);
            }

            $time = $guru->presensi->mapToGroups(function ($item) {
                return [$item->keterangan => $item->time];
            });
            
            $jam_kerja_actual =  $proses->sumJamKerjaActual($time);
            //cek keterlambatan
            $keterlambatan = $proses->keterlambatan($time);
            //cek Pulang Cepat
            $pulang_cepat = $proses->pulangCepat($time);
            //lembur
            $jam_lembur = $proses->jamLembur($time);
            //kehadiran
            $kehadiran = $proses->kehadiran($time);

            $coll = ([
                'ijin' => 0,
                'dinas' => 0,
                'id' => $guru->id,
                'nama' => $guru->nama,
                'jabatan' => $guru->jabatan,
                'bulan' => $month,
                'jam_kerja_standart' => $guru->presensi->count() * 6.5 / 2,
                'jam_kerja_actual' => $jam_kerja_actual,
                'total_terlambat' => $keterlambatan[0],
                'menit_terlambat' => floor($keterlambatan[1] / 60),
                'total_pulang_cepat' => $pulang_cepat[0],
                'menit_pulang_cepat' => floor($pulang_cepat[1] / 60),
                'jam_lembur' => floor($jam_lembur / (60*60)),
                'kehadiran_standart' => $kehadiran[0],
                'kehadiran_actual' => $kehadiran[1],
                'absen' => $kehadiran[2],
                'jam_pulang' => $proses->keluar,
                'batas_lembur' => $proses->batas_lembur
            ]);
        } else {
            return view('individu.index')->with('users', UserModel::all())->with('guru', null);
        }
        // dd($coll);

        return view('individu.index')->with('users', UserModel::all())
                                    ->with('guru', $coll)
                                    ->with('jam', $time)
                                    ->with('jam_pulang', $proses->keluar)
                                    ->with('batas_lembur', $proses->batas_lembur);
    }

    public function export(Request $req)
    {
        $proses = new ViewDataController();
        $bulan = $req->bulan;
        
        if (!empty($bulan)) {
            $data = UserModel::whereHas('presensi', function ($query) use ($bulan) {
                $query->where("bulan", "=", $bulan);
            })->with(['presensi'=> function ($query) use ($bulan) {
                $query->where('presensi_models.bulan', '=', $bulan);
            }])->get();

            if ($data == null) {
                return view('individu.index')->with('users', UserModel::all())->with('guru', null);
            }
            $presensi_bulanan = $data->map(function ($guru, $key) use ($proses) {
                $time = $guru->presensi->mapToGroups(function ($item) {
                    return [$item->keterangan => $item->time];
                });
                
                $jam_kerja_actual =  $proses->sumJamKerjaActual($time);
                //cek keterlambatan
                $keterlambatan = $proses->keterlambatan($time);
                //cek Pulang Cepat
                $pulang_cepat = $proses->pulangCepat($time);
                //lembur
                $jam_lembur = $proses->jamLembur($time);
                //kehadiran
                $kehadiran = $proses->kehadiran($time);

                return [
                    'ijin' => 0,
                    'dinas' => 0,
                    'id' => $guru->id,
                    'nama' => $guru->nama,
                    'jabatan' => $guru->jabatan,
                    'bulan' => date('n', strtotime($time['Datang'][0])),
                    'jam_kerja_standart' => $guru->presensi->count() * 6.5 / 2,
                    'jam_kerja_actual' => $jam_kerja_actual,
                    'total_terlambat' => $keterlambatan[0],
                    'menit_terlambat' => floor($keterlambatan[1] / 60),
                    'total_pulang_cepat' => $pulang_cepat[0],
                    'menit_pulang_cepat' => floor($pulang_cepat[1] / 60),
                    'jam_lembur' => floor($jam_lembur / (60*60)),
                    'kehadiran_standart' => $kehadiran[0],
                    'kehadiran_actual' => $kehadiran[1],
                    'absen' => $kehadiran[2],
                    'time' => $time
                ];
            });
        }

        // dd($presensi_bulanan);

        $data_export = [
            // 'users' => $user,
            'coll' => $presensi_bulanan,
            // 'time' => $time,
            'jam_pulang' => $proses->keluar,
            'batas_lembur' => $proses->batas_lembur
        ];
        // return view('individu.export')->with('data', $presensi_bulanan)
        //                             ->with('jam_pulang', $proses->keluar)
        //                             ->with('batas_lembur', $proses->batas_lembur);

        return Excel::download(new IndividuExport($data_export), 'Rekap_Perorangan.xlsx');
    }
}
