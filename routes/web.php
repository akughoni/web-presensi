<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PresensiController@index')->name('presensi.index');
Route::post('/presensi', 'PresensiController@storeCreate')->name('presensi');
Route::get('/rekapan', 'ViewDataController@index')->name('lihat.data');
Route::get('/rekapan/export', 'ViewDataController@export')->name('rekapan.export');
Route::get('/individu', 'IndividuController@index')->name('individu.data');
Route::get('/individu/export', 'IndividuController@export')->name('individu.export');
Route::get('/guru', 'GuruController@index')->name('guru');
Route::post('/guru/create', 'GuruController@create')->name('guru.add');
Route::get('/guru/delete/{id}', 'GuruController@delete')->name('guru.delete');
Route::get('/lihat-absen', 'PresensiController@show')->name('lihat.presensi');
Route::get('/lihat-absen/delete/{id}', 'PresensiController@delete')->name('delete.presensi');
